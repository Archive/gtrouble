/* htmlout.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "libguide.h"

extern int errno;

void
html_out_person (FILE * fp, personPtr cur)
{

  GUIDETRACE (("html_out_person\n"));
  if ((fp) && (cur))
    {
      fprintf (fp, "<ul>\n");
      if (cur->name)
	fprintf (fp, "<li>Contact Type/Role: %s\n", cur->type);
      if (cur->name)
	fprintf (fp, "<li>Name: %s\n", cur->name);
      if (cur->email)
	fprintf (fp, "<li>Email: <a href=\"mailto:%s\">%s</a>\n", cur->email,
		 cur->email);
      if (cur->company)
	fprintf (fp, "<li>Company: %s\n", cur->company);
      if (cur->organisation)
	fprintf (fp, "<li>Organisation: %s\n", cur->organisation);
      if (cur->snailmail)
	fprintf (fp, "<li>Snail Mail: %s\n", cur->snailmail);
      if (cur->webpage)
	fprintf (fp, "<li>Web Site: <a href=\"%s\">%s</a>\n", cur->webpage,
		 cur->webpage);
      if (cur->phone)
	fprintf (fp, "<li>Phone: %s\n", cur->phone);
      fprintf (fp, "</ul>\n");
    }
}

void
html_out_question (gchar * subdir, questionPtr cur)
{
  FILE *fp;
  gchar *buf;
  static int counter = 1;
  int i = 0;

  GUIDETRACE (("html_out_question\n"));

  if (subdir)
    {
      if (cur->ref)
	buf = g_strdup_printf ("%s/%s.html", subdir, cur->ref);
      else
	buf = g_strdup_printf ("%s/guide_page%d.html", subdir, counter++);
    }
  else
    {
      if (cur->ref)
	buf = g_strdup_printf ("%s.html", cur->ref);
      else
	buf = g_strdup_printf ("guide_page%d.html", counter++);
    }

  fp = fopen (buf, "w");
  g_free (buf);

  if ((fp) && (cur))
    {
      html_out_create_headers (fp, cur->ref);
      fprintf (fp, "<!-- Here is a question record -->\n");
      fprintf (fp, "%s\n<p><p>\n", cur->text);
      fprintf (fp, "<ul>\n");
      for (i = 0; i < cur->nbchoices; i++)
	html_out_choice (fp, cur->choices[i]);
      fprintf (fp, "</ul>\n");

      if (cur->tellme)
	{
	  fprintf (fp, "<hr>\nTell me more:\n<br>\n");
	  fprintf (fp, "%s\n<br>\n", cur->tellme);
	}

      fprintf (fp, "<!-- End of question record -->\n");
      html_out_create_footers (fp);
    }

  fclose (fp);

}

void
html_out_choice (FILE * fp, choicePtr cur)
{
  GUIDETRACE (("html_out_choice\n"));
  if ((fp) && (cur))
    {
      gchar *linkbuf = NULL;
      if ((!strcasecmp (cur->action, "goto")) && (cur->target))
	linkbuf =
	  g_strdup_printf ("<li><a href=\"%s.html\">%s</a>\n", cur->target,
			   cur->label);
      else if ((!strcasecmp (cur->action, "ext_link")) && (cur->target))
	linkbuf =
	  g_strdup_printf ("<li><a href=\"%s\">%s</a>\n", cur->target,
			   cur->label);
      else
	linkbuf = g_strdup_printf ("<li>%s\n", cur->label);
      fprintf (fp, linkbuf);
    }
}


char *
html_out_guide (guidePtr cur)
{
  FILE *fp;
  int i = 0;
  gchar *directory = NULL;
  gchar *filename = NULL;
  static gchar *ret = NULL;
  GUIDETRACE (("html_out_guide\n"));

  if (cur)
    {
      if (cur->guideID)
	{
	  directory = g_strdup_printf ("%s", cur->guideID);
	}
      else
	{
	  directory = g_strdup_printf ("guide_output");
	}

      errno = 0;
      if (mkdir (directory, 0700) == -1)
	{
	  if (errno == EEXIST)
	    /* Continue, this means the dir exists already */
	    ;
	  else
	    {
	      GUIDETRACE (("Couldn't create directory\n"));
	      perror ("Dir error: ");
	      exit (2);
	    }
	}

      filename = g_strdup_printf ("%s/index.html", directory);
      ret = g_strdup (filename);

      fp = fopen (filename, "w");
      if (fp == NULL)
	{
	  GUIDETRACE (("Couldn't create file\n"));
	  perror ("File error: ");
	  exit (2);
	}

      fprintf (fp, "<!-- Here starts the guide -->\n");

      html_out_create_headers (fp, cur->topic);

      if (cur->topic)
	fprintf (fp, "<h2>Topic - %s</h2>\n\n", cur->topic);
      else
	fprintf (fp, "<h2>Guide has no topic!</h2>\n\n");

      if (cur->guideID)
	fprintf (fp, "<b>GuideID:</b> %s\n<br>\n", cur->guideID);

      if (cur->status)
	fprintf (fp, "<b>Status:</b> %s\n<br>\n", cur->status);

      if (cur->modified)
	fprintf (fp, "<b>Modified:</b> %s\n<br>\n", cur->modified);

      if (cur->category)
	fprintf (fp, "<b>Category:</b> %s\n<br>\n", cur->category);

      if (cur->application)
	fprintf (fp, "<h3>Application - %s</h3>\n", cur->application);

      if (cur->intro)
	fprintf (fp, "<b>Introduction:</b> %s\n<p>\n", cur->intro);

      /* Stick first question here? Maybe later... */

      if (cur->questions[0]->ref)
	{
	  gchar *buf = g_strdup_printf ("%s.html", cur->questions[0]->ref);
	  fprintf (fp, "<p>\n<a href=\"%s\">Click here to start the Guide</a>"
		   "\n<p>\n<p>", buf);
	  g_free (buf);
	}

      for (i = 0; i < cur->nbquestions; i++)
	html_out_question (directory, cur->questions[i]);

      if (cur->nbcontacts > 0)
	fprintf (fp, "<hr>\n<br>\nFor feedback or further information on "
		 "this guide or the application it to, please contact:\n<p>\n");

      for (i = 0; i < cur->nbcontacts; i++)
	{
	  fprintf (fp, "<br>\n<br>\n");
	  html_out_person (fp, cur->contacts[i]);
	  fprintf (fp, "<br>\n");
	}

      html_out_create_footers (fp);

      fclose (fp);
      g_free (directory);
      g_free (filename);
    }
  return ret;
}


void
html_out_gguide (gguidePtr cur)
{
  int i;
  gchar *buffer;
  gchar *sys;
  GUIDETRACE (("html_out_guide\n"));
  /*
   * Do whatever you want and free the structure.
   */
  for (i = 0; i < cur->nbguides; i++)
    {
      buffer = g_strconcat ("`pwd`/", html_out_guide (cur->guides[i]), NULL);
      sys = g_strdup_printf ("gnome-moz-remote %s", buffer);
      system (sys);
      g_free (sys);
      g_free (buffer);
    }
}


void
html_out_create_headers (FILE * fp, gchar * title)
{
  GUIDETRACE (("html_out_create_headers\n"));
  fprintf (fp,
	   "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n");
  fprintf (fp,
	   "<head>\n<meta http-equiv=\"Content-Type\" "
	   "content=\"text/html; charset=iso-8859-1\">\n");

  if (title)
    fprintf (fp, "<title>libGuide produced html - %s</title>\n", title);
  else
    fprintf (fp, "<title>libGuide produced html</title>\n");

  fprintf (fp, "</head>\n<body>\n");

}

void
html_out_create_footers (FILE * fp)
{
  GUIDETRACE (("html_out_create_footers\n"));
  fprintf (fp, "\n</body>\n</html>");
}
