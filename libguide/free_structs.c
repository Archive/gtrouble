/* freestructs.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "libguide.h"
extern int libguide_debug;

void
guide_free_gguide (gguidePtr cur)
{
  int i = 0;
  GUIDETRACE (("guide_free_gguide size:%d\n", sizeof (gguide)));

  if (cur)
    {
      for (i = 0; i < cur->nbguides; i++)
	guide_free_guide (cur->guides[i]);
      g_free (cur);
    }
}

void
guide_free_guide (guidePtr cur)
{
  int i = 0;
  GUIDETRACE (("guide_free_guide size:%d\n", sizeof (guide)));

  if (cur)
    {
      for (i = 0; i < cur->nbcontacts; i++)
	guide_free_person (cur->contacts[i]);
      for (i = 0; i < cur->nbquestions; i++)
	guide_free_question (cur->questions[i]);
      if (cur->guideID)
	g_free (cur->guideID);
      if (cur->status)
	g_free (cur->status);
      if (cur->modified)
	g_free (cur->modified);
      if (cur->category)
	g_free (cur->category);
      if (cur->topic)
	g_free (cur->topic);
      if (cur->intro)
	g_free (cur->intro);
      g_free (cur);
    }
}

void
guide_free_question (questionPtr cur)
{
  int i = 0;
  GUIDETRACE (("guide_free_question size:%d\n", sizeof (question)));

  if (cur)
    {
      for (i = 0; i < cur->nbchoices; i++)
	guide_free_choice (cur->choices[i]);
      if (cur->test)
	guide_free_test (cur->test);
      if (cur->ref)
	g_free (cur->ref);
      if (cur->text)
	g_free (cur->text);
      if (cur->longtext)
	g_free (cur->longtext);
      if (cur->tellme)
	g_free (cur->tellme);
      g_free (cur);
    }
}

void
guide_free_choice (choicePtr cur)
{
  GUIDETRACE (("guide_free_choice size:%d\n", sizeof (choice)));

  if (cur)
    {
      if (cur->label)
	g_free (cur->label);
      if (cur->action)
	g_free (cur->action);
      if (cur->target)
	g_free (cur->target);
      if (cur->value)
	g_free (cur->value);
      g_free (cur);
    }
}

void
guide_free_person (personPtr cur)
{
  GUIDETRACE (("guide_free_person size:%d\n", sizeof (person)));

  if (cur)
    {
      if (cur->type)
	g_free (cur->type);
      if (cur->name)
	g_free (cur->name);
      if (cur->email)
	g_free (cur->email);
      if (cur->company)
	g_free (cur->company);
      if (cur->organisation)
	g_free (cur->organisation);
      if (cur->snailmail)
	g_free (cur->snailmail);
      if (cur->webpage)
	g_free (cur->webpage);
      if (cur->phone)
	g_free (cur->phone);
      g_free (cur);
    }
}

void
guide_free_test (testPtr cur)
{
  GUIDETRACE (("guide_free_test size:%d\n", sizeof (test)));

  if (cur)
    {
      if (cur->text)
	g_free (cur->text);
      if (cur->lang)
	g_free (cur->lang);
      if (cur->script)
	g_free (cur->script);
      g_free (cur);
    }
}
