/* guide_parse.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "libguide.h"

extern int libguide_debug;

/* The code to parse a person structure */
personPtr
parsePerson (xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, guidePtr parent)
{
  personPtr ret = NULL;

  GUIDETRACE (("parsePerson\n"));

  /* allocate the struct */
  ret = (personPtr) malloc (sizeof (person));
  if (ret == NULL)
    {
      fprintf (stderr, "out of memory\n");
      return (NULL);
    }
  memset (ret, 0, sizeof (person));

  ret->parent = parent;

  cur = cur->childs;
  while (cur != NULL)
    {
      if ((!strcasecmp (cur->name, "Type")) && (cur->ns == ns))
	ret->type = xmlNodeGetContent (cur->childs);
      else if ((!strcasecmp (cur->name, "Person")) && (cur->ns == ns))
	ret->name = xmlNodeGetContent (cur->childs);
      else if ((!strcasecmp (cur->name, "Email")) && (cur->ns == ns))
	ret->email = xmlNodeGetContent (cur->childs);
      else if ((!strcasecmp (cur->name, "Company")) && (cur->ns == ns))
	ret->company = xmlNodeGetContent (cur->childs);
      else if (((!strcasecmp (cur->name, "Organisation"))
		|| (!strcasecmp (cur->name, "Organization")))
	       && (cur->ns == ns))
	ret->organisation = xmlNodeGetContent (cur->childs);
      else if (((!strcasecmp (cur->name, "WebPage"))
		|| (!strcasecmp (cur->name, "WebSite"))) && (cur->ns == ns))
	ret->webpage = xmlNodeGetContent (cur->childs);
      else if (((!strcasecmp (cur->name, "SnailMail"))
		|| (!strcasecmp (cur->name, "Post"))) && (cur->ns == ns))
	ret->snailmail = xmlNodeGetContent (cur->childs);
      else if (((!strcasecmp (cur->name, "Phone"))
		|| (!strcasecmp (cur->name, "Tel"))) && (cur->ns == ns))
	ret->phone = xmlNodeGetContent (cur->childs);
      else
	{
	  fprintf (stderr,
		   "In Person parser. Entity not recognised! Name -->%s<--\n",
		   cur->name);
	}

      cur = cur->next;
    }
  return (ret);
}


/* The code to parse a person structure */
testPtr
parse_test (xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, questionPtr parent)
{
  testPtr ret = NULL;

  GUIDETRACE (("parse_test\n"));

  /* allocate the struct */
  ret = (testPtr) malloc (sizeof (test));
  if (ret == NULL)
    {
      fprintf (stderr, "out of memory\n");
      return (NULL);
    }
  memset (ret, 0, sizeof (test));

  ret->parent = parent;

  if (cur)
    if ((!strcmp (cur->name, "Test")) && (cur->ns == ns))
      {
	ret->lang = xmlGetProp (cur, "lang");
	if (ret->lang == NULL)
	  fprintf (stderr, "Test has no lang\n");
      }

  cur = cur->childs;
  while (cur != NULL)
    {
      if (((!strcasecmp (cur->name, "Testtext"))
	   || (!strcasecmp (cur->name, "text"))) && (cur->ns == ns))
	guide_get_i18n_string (doc, cur->childs, &(ret->text));
      else if (((!strcasecmp (cur->name, "Testscript"))
		|| (!strcasecmp (cur->name, "script"))) && (cur->ns == ns))
	ret->script = xmlNodeGetContent (cur->childs);
      else
	{
	  fprintf (stderr,
		   "In Test Parser. Entity not recognised! Name -->%s<--\n",
		   cur->name);
	}

      cur = cur->next;
    }
  return (ret);
}


/* The code to parse a choice structure */
choicePtr
parse_choice (xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, questionPtr parent)
{
  choicePtr ret = NULL;

  GUIDETRACE (("parse_choice\n"));

  if (!cur)
    return NULL;

  /* allocate the struct */
  ret = (choicePtr) malloc (sizeof (choice));
  if (ret == NULL)
    {
      fprintf (stderr, "out of memory\n");
      exit (2);
    }
  memset (ret, 0, sizeof (choice));

  ret->parent = parent;

  ret->value = xmlGetProp (cur, "value");
  if (ret->value == NULL)
    fprintf (stderr, "Choice has no value\n");
  ret->action = xmlGetProp (cur, "action");
  if (ret->action == NULL)
    fprintf (stderr, "Choice has no action\n");
  ret->target = xmlGetProp (cur, "target");

  cur = cur->childs;

  while (cur)
    {
      if (((!strcasecmp (cur->name, "Choicelabel"))
	   || (!strcasecmp (cur->name, "label"))) && (cur->ns == ns))
	guide_get_i18n_string (doc, cur->childs, &(ret->label));
      else
	{
	  fprintf (stderr,
		   "In Choice Parser. Entity not recognised! Name -->%s<--\n",
		   cur->name);
	}
      cur = cur->next;
    }
  return (ret);
}


/* Parse a question */
questionPtr
parse_question (xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, guidePtr parent,
		gboolean reset)
{
  static int counter = 1;
  questionPtr ret = NULL;

  GUIDETRACE (("parseQuestion\n"));

  if (!cur)
    return NULL;

  if (reset)
    counter = 1;

  /*
   * allocate the struct
   */
  ret = (questionPtr) malloc (sizeof (question));
  if (ret == NULL)
    {
      fprintf (stderr, "out of memory\n");
      return (NULL);
    }
  memset (ret, 0, sizeof (question));

  ret->ref = xmlGetProp (cur, "ref");
  if (ret->ref == NULL)
    {
      fprintf (stderr, "Question has no Ref\n");
    }

  ret->parent = parent;

  cur = cur->childs;
  while (cur)
    {
      if ((!strcasecmp (cur->name, "text")) && (cur->ns == ns))
	guide_get_i18n_string (doc, cur->childs, &(ret->text));
      else if (((!strcasecmp (cur->name, "TellMe"))
		|| (!strcasecmp (cur->name, "tellmetext")))
	       && (cur->ns == ns))
	guide_get_i18n_string (doc, cur->childs, &(ret->tellme));
      else if (((!strcasecmp (cur->name, "LongText"))
		|| (!strcasecmp (cur->name, "long"))) && (cur->ns == ns))
	guide_get_i18n_string (doc, cur->childs, &(ret->longtext));
      else if ((!strcasecmp (cur->name, "test")) && (cur->ns == ns))
	ret->test = parse_test (doc, ns, cur, ret);
      else if ((!strcasecmp (cur->name, "choice")) && (cur->ns == ns))
	{
	  choicePtr choice = parse_choice (doc, ns, cur, ret);
	  if (choice != NULL)
	    ret->choices[ret->nbchoices++] = choice;
	  if (ret->nbchoices >= 100)
	    break;
	}
      else
	{
	  fprintf (stderr,
		   "In Question Parser. Entity not recognised! Name -->%s<--\n",
		   cur->name);
	}
      cur = cur->next;
    }
  ret->int_ref = counter++;
  return (ret);
}


/* Parse a Guide */
guidePtr
parse_guide (xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, gguidePtr parent)
{
  guidePtr ret = NULL;
  gboolean first = TRUE;

  GUIDETRACE (("parse_guide\n"));

  /* allocate the struct */
  ret = (guidePtr) malloc (sizeof (guide));
  if (ret == NULL)
    {
      fprintf (stderr, "out of memory\n");
      return (NULL);
    }
  memset (ret, 0, sizeof (guide));

  if (cur)
    if ((!strcasecmp (cur->name, "Guide")) && (cur->ns == ns))
      {
	ret->guideID = xmlGetProp (cur, "ID");
	if (ret->guideID == NULL)
	  {
	    fprintf (stderr, "Guide has no ID\n");
	  }
      }

  ret->parent = parent;

  cur = cur->childs;
  while (cur != NULL)
    {
      if (
	  ((!strcasecmp (cur->name, "Application"))
	   || (!strcasecmp (cur->name, "app"))
	   || (!strcasecmp (cur->name, "program"))) && (cur->ns == ns))
	ret->application = xmlNodeGetContent (cur->childs);
      else if ((!strcasecmp (cur->name, "Status")) && (cur->ns == ns))
	ret->status = xmlNodeGetContent (cur->childs);
      else if ((!strcasecmp (cur->name, "Modified")) && (cur->ns == ns))
	ret->modified = xmlNodeGetContent (cur->childs);
      else if ((!strcasecmp (cur->name, "Category")) && (cur->ns == ns))
	ret->category = xmlNodeGetContent (cur->childs);
      else if ((!strcasecmp (cur->name, "Topic")) && (cur->ns == ns))
	ret->topic = xmlNodeGetContent (cur->childs);
      else if ((!strcasecmp (cur->name, "Intro")) && (cur->ns == ns))
	guide_get_i18n_string (doc, cur->childs, &(ret->intro));
      else if (((!strcasecmp (cur->name, "Question"))
		|| (!strcasecmp (cur->name, "Answer"))
		|| (!strcasecmp (cur->name, "node"))) && (cur->ns == ns))
	{
	  questionPtr question = parse_question (doc, ns, cur, ret, first);
	  first = FALSE;
	  if (question != NULL)
	    ret->questions[ret->nbquestions++] = question;
	  if (ret->nbquestions >= 500)
	    break;
	}
      else if ((!strcasecmp (cur->name, "Contact")) && (cur->ns == ns))
	{
	  personPtr person = parsePerson (doc, ns, cur, ret);
	  if (person != NULL)
	    ret->contacts[ret->nbcontacts++] = person;
	  if (ret->nbcontacts >= 500)
	    break;
	}
      /* For some reason, comments generate entities with name->"text" */
      else if (strcmp (cur->name, "text"))
	{
	  fprintf (stderr,
		   "In Guide Parser. Entity not recognised! Name -->%s<--\n",
		   cur->name);
	  fprintf (stderr, "                 Content -->%s<--\n",
		   xmlNodeGetContent (cur->childs));
	  fprintf (stderr, "                 Previous Name -->%s<--\n",
		   cur->prev->name);
	  fprintf (stderr, "                 Previous Content -->%s<--\n",
		   xmlNodeGetContent (cur->prev));
	  fprintf (stderr, "                 Next Name -->%s<--\n",
		   cur->next->name);
	  fprintf (stderr, "                 Next Content -->%s<--\n",
		   xmlNodeGetContent (cur->next));
	  fprintf (stderr, "                 Parent Name -->%s<--\n",
		   cur->parent->name);
	  fprintf (stderr, "                 Parent Content -->%s<--\n",
		   xmlNodeGetContent (cur->parent));
	}
      cur = cur->next;
    }
  return (ret);
}

gguidePtr
parse_gguide_mem (char *xmltext)
{
  return _parse_gguide_multi (xmltext, TRUE);
}

gguidePtr
parse_gguide_file (char *xmlfilename)
{
  return _parse_gguide_multi (xmlfilename, FALSE);
}

gguidePtr
_parse_gguide_multi (char *xmltext, gboolean from_mem)
{
  xmlDocPtr doc;
  gguidePtr ret;
  guidePtr guide;
  xmlNsPtr ns;
  xmlNodePtr cur;
  xmlDtdPtr dtd;

  gchar *xmlcpy;

  xmlcpy = g_strdup (xmltext);

/*  dtd=xmlParseDTD("../guides/guide.dtd","../guides/guide.dtd");  */

  
  /* build an XML tree from the file/string  */
  if (from_mem)
    doc = xmlParseMemory (xmltext, strlen (xmlcpy) + 1);
  else
    doc = xmlParseFile (xmltext);
  if (doc == NULL)
    return (NULL);

  g_free (xmlcpy);

  /* Check the document is of the right kind */
  cur = doc->root;
  if (cur == NULL)
    {
      fprintf (stderr, "empty document\n");
      xmlFreeDoc (doc);
      return (NULL);
    }
  ns =
    xmlSearchNsByHref (doc, cur, "http://www.gnome.org/guide-spec-location");
  if (ns == NULL)
    {
      fprintf (stderr,
	       "document of the wrong type, Guide Namespace not found\n");
      xmlFreeDoc (doc);
      return (NULL);
    }
  if (strcasecmp (cur->name, "Guide"))
    {
      fprintf (stderr, "document of the wrong type, root node != Guide\n");
      xmlFreeDoc (doc);
      return (NULL);
    }

  /* Allocate the structure to be returned. */
  ret = (gguidePtr) malloc (sizeof (gguide));
  if (ret == NULL)
    {
      fprintf (stderr, "out of memory\n");
      xmlFreeDoc (doc);
      return (NULL);
    }
  memset (ret, 0, sizeof (gguide));

  /* Now, walk the tree. */
  /* First level we expect just Guide(s) */
  if ((strcasecmp (cur->name, "Guide")) || (cur->ns != ns))
    {
      fprintf (stderr, "document of the wrong type, Guide expected\n");
      xmlFreeDoc (doc);
      free (ret);
      return (NULL);
    }

  while (cur != NULL)
    {
      if ((!strcasecmp (cur->name, "Guide")) && (cur->ns == ns))
	{
	  guide = parse_guide (doc, ns, cur, ret);
	  if (guide != NULL)
	    ret->guides[ret->nbguides++] = guide;
	  if (ret->nbguides >= 500)
	    break;
	}
      cur = cur->next;
    }

  xmlFreeDoc (doc);

  return (ret);
}

void
guide_get_i18n_string (xmlDocPtr doc, xmlNodePtr child, gchar ** current)
{
  gchar *locallang = guide_get_lang ();
  gchar *temp = xmlNodeGetContent (child);
  GUIDETRACE (("guide_get_i18n_string\n"));
  if (*current)
    {
      /* Already have some text, so it must be either in C or
       * our locale. Only replace this if the tag says its our
       * locale */
      gchar *langval = xmlGetProp (child->parent, "xml:lang");
      if ((langval) && (!strcmp (langval, locallang)))
	{
	  g_free (*current);
	  *current = g_strdup (temp);
	  g_free (temp);
	}
    }
  else
    {
      /* We don't have any text yet, so grab this if it is in C,
       * has no lang specified, or is the lang for our locale */
      gchar *langval = xmlGetProp (child->parent, "xml:lang");
      if ((langval == NULL) || (!strcasecmp (langval, locallang))
	  || (!strcasecmp (langval, "C")))
	{
	  *current = g_strdup (temp);
	  g_free (temp);
	}
    }
}

gchar *
guide_get_lang ()
{
  static gchar ret[2] = { 'C', '\n' };
  gchar *temp;
  temp = getenv ("LANG");
  if (temp)
    return temp;
  else
    return ret;
}

int
guide_get_int_ref (gchar * targetname, guidePtr guideroot)
{
  int i = 0;

  for (i = 0; i < guideroot->nbquestions; i++)
    if (!strcmp (guideroot->questions[i]->ref, targetname))
      return guideroot->questions[i]->int_ref;

  return -1;
}
