/* htmlstring.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "libguide.h"

void
html_string_person (GString * string, personPtr cur)
{
  GUIDETRACE (("html_string_person\n"));

  if (cur)
    {
      g_string_append (string, "<ul>\n");
      if (cur->type)
	g_string_sprintfa (string, "<li>Contact Type: %s\n", cur->type);
      if (cur->name)
	g_string_sprintfa (string, "<li>Name: %s\n", cur->name);
      if (cur->email)
	g_string_sprintfa (string,
			   "<li>Email: <a href=\"mailto:%s\">%s</a>\n",
			   cur->email, cur->email);
      if (cur->company)
	g_string_sprintfa (string, "<li>Company: %s\n", cur->company);
      if (cur->organisation)
	g_string_sprintfa (string, "<li>Organisation: %s\n",
			   cur->organisation);
      if (cur->snailmail)
	g_string_sprintfa (string, "<li>Snail Mail: %s\n", cur->snailmail);
      if (cur->webpage)
	g_string_sprintfa (string, "<li>Web Site: <a href=\"%s\">%s</a>\n",
			   cur->webpage, cur->webpage);
      if (cur->phone)
	g_string_sprintfa (string, "<li>Phone: %s\n", cur->phone);

      g_string_append (string, "</ul>\n");
    }
}

GString *
html_string_question (questionPtr cur)
{
  GUIDETRACE (("html_string_question\n"));

  if (cur)
    {
      int i = 0;
      GString *string = g_string_new (NULL);

      html_string_create_headers (string, cur->ref);

      g_string_sprintfa (string, "<font size=\"+1\"><b>%s</b></font>\n<p>\n",
			 cur->text);
      g_string_append (string, "<ul>\n");

      for (i = 0; i < cur->nbchoices; i++)
	{
	  html_string_choice (string, cur->choices[i]);
	}

      g_string_append (string, "</ul>\n");

      if (cur->tellme)
	{
	  g_string_append (string,
			   "<hr><font size=\"+1\"><b>\nTell me more:</b></font>\n<br>\n");
	  g_string_sprintfa (string, "%s\n<br>\n", cur->tellme);
	}

      if (cur->longtext)
	{
	  g_string_append (string,
			   "<hr><font size=\"+1\"><b>\nLonger Description:</b></font>\n<br>\n");
	  g_string_sprintfa (string, "%s\n<br>\n", cur->longtext);
	}

      if (cur->test)
	{
	  g_string_sprintfa (string, "<br><hr><p>%s</p>", cur->test->text);
	  g_string_sprintfa (string,
			     "<a href=\"guidetest:%s\">"
			     "Click here to run the test</a>",
			     cur->test->lang);
	}

      g_string_append (string,
		       "<p><hr>\n<a href=\"pipe:0\">Restart this guide</a><p>\n");

      html_string_create_footers (string);

      return string;
    }
  return NULL;
}

void
html_string_choice (GString * string, choicePtr cur)
{
  GUIDETRACE (("html_string_choice\n"));
  if (cur)
    {
      if ((!strcasecmp (cur->action, "goto")) && (cur->target))
	g_string_sprintfa (string, "<li><a href=\"pipe:%d\">%s</a><p>\n",
			   guide_get_int_ref (cur->target,
					      cur->parent->parent),
			   cur->label);
      else if ((!strcasecmp (cur->action, "ext_link")) && (cur->target))
	g_string_sprintfa (string, "<li><a href=\"%s\">%s</a><p>\n",
			   cur->target, cur->label);
      else
	g_string_sprintfa (string, "<li>%s<p>\n", cur->label);
    }
}

GString **
html_string_guide (guidePtr cur)
{
  int i = 0;

  GString *string = g_string_new (NULL);

  GUIDETRACE (("html_string_guide\n"));

  if (cur)
    {
      GString **ret = malloc (sizeof (GString) * cur->nbquestions + 1);

      html_string_create_headers (string, cur->topic);

      if (cur->application)
	g_string_sprintfa (string, "<h2>Application - %s</h2>\n",
			   cur->application);

      if (cur->topic)
	g_string_sprintfa (string, "<h3>Topic - %s</h3>\n\n", cur->topic);

      if (cur->guideID)
	g_string_sprintfa (string, "<b>GuideID:</b> %s\n<br>\n",
			   cur->guideID);

      if (cur->status)
	g_string_sprintfa (string, "<b>Status:</b> %s\n<br>\n", cur->status);

      if (cur->modified)
	g_string_sprintfa (string, "<b>Modified:</b> %s\n<br>\n",
			   cur->modified);

      if (cur->category)
	g_string_sprintfa (string, "<b>Category:</b> %s\n<br>\n",
			   cur->category);

      if (cur->intro)
	g_string_sprintfa (string, "<br><b>Introduction:</b> %s\n<br>\n",
			   cur->intro);

      /* Stick first question here? Maybe later... */

      if (cur->questions[0]->ref)
	{
	  g_string_append (string, "<p>\n<a href=\"pipe:1\">Click here "
			   "to start the Guide</a>\n</p>");
	}

      if (cur->nbcontacts > 0)
	{
	  g_string_append (string,
			   "<hr>\nFor feedback or further information on "
			   "this guide or the application it to, please contact:"
			   "\n<br>\n");

	  for (i = 0; i < cur->nbcontacts; i++)
	    {
	      g_string_append (string, "<p>\n");
	      html_string_person (string, cur->contacts[i]);
	      g_string_append (string, "</p>\n");
	    }
	}

      html_string_create_footers (string);

      ret[0] = g_string_new (string->str);

      g_string_free (string, TRUE);

      for (i = 0; i < cur->nbquestions; i++)
	{
	  GString *buf = html_string_question (cur->questions[i]);
	  ret[i + 1] = g_string_new (buf->str);
	  g_string_free (buf, TRUE);
	}

      return ret;
    }
  return NULL;
}

guideHtmlDocument *
guide_create_html_document (guidePtr cur)
{
  static guideHtmlDocument *ret;
  ret = malloc (sizeof (guideHtmlDocument));

  ret->pages = html_string_guide (cur);
  ret->guide_struct = cur;

  return ret;
}

void
html_string_create_headers (GString * string, gchar * title)
{
  GUIDETRACE (("html_string_create_headers\n"));

  g_string_append (string,
		   "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n"
		   "<html><head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; "
		   "charset=iso-8859-1\">\n");

  if (title)
    g_string_sprintfa (string,
		       "<title>libGuide produced html - %s</title>\n</head>\n<body bgcolor=\"#FFFFFF\" background=\"%sguide_bg.jpg\" ALINK=\"#008800\">\n",
		       title, GUIDEDIR);
  else
    g_string_sprintfa (string,
		       "<title>libGuide produced html</title>\n</head>\n<body bgcolor=\"#FFFFFF\" background=\"%sguide_bg.jpg\" ALINK=\"#008800\">\n",
		       GUIDEDIR);
}

void
html_string_create_footers (GString * string)
{
  GUIDETRACE (("html_string_create_footers\n"));

  g_string_append (string, "\n</body>\n</html>");
}
