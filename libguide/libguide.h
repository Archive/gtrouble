#ifndef LIBGUIDE_H
#define LIBGUIDE_H

#include <glib.h>
#include <gnome-xml/parser.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <ctype.h>
#include "structs.h"

#define GUIDE_DEBUG

#ifdef GUIDE_DEBUG
#define GUIDETRACE(a) { \
    printf("%s +%u : ",__FILE__,__LINE__); \
            printf a; \
            fflush(stdout); \
}
#else
#define GUIDETRACE(a) { \
    /* No debug */; \
}
#endif

personPtr parsePerson (xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, guidePtr parent);
questionPtr parse_question (xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, guidePtr parent, gboolean reset);
guidePtr parse_guide (xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, gguidePtr parent);
gguidePtr parse_gguide_mem (char *xmltext);
gguidePtr parse_gguide_file (char *xmlfilename);
gguidePtr _parse_gguide_multi (char *xmltext,gboolean from_mem);
choicePtr parse_choice (xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, questionPtr parent);
testPtr parse_test (xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, questionPtr parent);
void guide_get_i18n_string (xmlDocPtr doc, xmlNodePtr child,
                            gchar ** current);
gchar *guide_get_lang (void);
void html_out_person (FILE * fp, personPtr cur);
void html_out_question (gchar * subdir, questionPtr cur);
void html_out_choice (FILE * fp, choicePtr cur);
gchar *html_out_guide (guidePtr cur);
void html_out_gguide (gguidePtr cur);
void html_out_create_headers (FILE * fp, gchar * title);
void html_out_create_footers (FILE * fp);
void guide_free_gguide (gguidePtr cur);
void guide_free_guide (guidePtr cur);
void guide_free_question (questionPtr cur);
void guide_free_choice (choicePtr cur);
void guide_free_person (personPtr cur);
void guide_free_test (testPtr cur);

void html_string_person (GString * string, personPtr cur);
GString * html_string_question (questionPtr cur);
void html_string_choice (GString * string, choicePtr cur);
GString ** html_string_guide (guidePtr cur);
void html_string_create_headers (GString * string, gchar * title);
void html_string_create_footers (GString * string);

int guide_get_int_ref (gchar * targetname, guidePtr guideroot);
guideHtmlDocument* guide_create_html_document (guidePtr cur);
void guide_export_to_html_files_gguide (gguidePtr cur);

#endif
