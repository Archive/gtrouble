/* structs.h
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/* Ok. Let's try and visualise this thing... */

/*
    Guides
    |
    +---Guide (multiple)
    |
    \---Guide
        |
        |---Status
	|---Modified
	|---Reference
	|---Application
	|---Category
	|---Topic
	|---Intro
	|---nbquestions
	|---nbcontacts
	|
	+---Contact (multiple)
	|
	|---Contact
	|   |
	|   |---type
        |   |---name
        |   |---email
	|   |---company
	|   |---organisation
	|   |---snailmail
	|   |---webpage
	|   \---phone
	|   
	+---Question (multiple)
	|
	\---Question
	    |
            |---Ref
	    |---Text
	    |---LongText
	    |---Tellme
	    |---Test
            |   |
	    |   |---Text
	    |   |---Lang
	    |   |---Script
	    |
	    |---nbchoices
	    |
	    +---Choice (multiple)
	    |
	    \---Choice
	        |
	        |---label
	        |---value
	        |---action
	        \---target
*/


typedef struct _question question;
typedef struct _question *questionPtr;
typedef struct _person person;
typedef struct _person *personPtr;
typedef struct _choice choice;
typedef struct _choice *choicePtr;
typedef struct _test test;
typedef struct _test *testPtr;
typedef struct _guide guide;
typedef struct _guide *guidePtr;
typedef struct _gguide gguide;
typedef struct _gguide *gguidePtr;
typedef struct _guideHtmlDocument guideHtmlDocument;

/* A question  */
struct _question
{
  char *ref;
  int int_ref;
  char *text;
  char *longtext;
  char *tellme;
  testPtr test;
  int nbchoices;
  choicePtr choices[100];	/* will use dynamic alloc */
  guidePtr parent;
};

/* A person record */
struct _person
{
  char *type;
  char *name;
  char *email;
  char *company;
  char *organisation;
  char *snailmail;
  char *webpage;
  char *phone;
  guidePtr parent;
};

/* A choice  */
struct _choice
{
  char *label;
  char *action;
  char *target;
  char *value;
  questionPtr parent;
};

/* A test record */
struct _test
{
  char *text;
  char *lang;
  char *script;
  questionPtr parent;
};

/* a Description for a guide  */
struct _guide
{
  char *guideID;
  char *status;
  char *modified;
  char *application;
  char *category;
  char *topic;
  char *intro;
  int nbquestions;
  questionPtr questions[500];	/* will use dynamic alloc */
  int nbcontacts;
  personPtr contacts[100];	/* will use dynamic alloc */
  gguidePtr parent;
};

/* A collection of Gnome Guides */
struct _gguide
{
  int nbguides;
  guidePtr guides[100];		/* will use dynamic alloc */
};


/* A high-level Html document, containing pages of html data, *and* the
 * associated structs */
struct _guideHtmlDocument
{
    GString  **pages;
    guidePtr     guide_struct;
};

