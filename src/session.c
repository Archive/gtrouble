/* session.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gTrouble.h"

/*the save_yourself handler, we can safely ignore most of the
   parameters, and just save our session and return TRUE*/
int
save_yourself (GnomeClient * client, int phase,
	       GnomeSaveStyle save_style, int shutdown,
	       GnomeInteractStyle interact_style, int fast,
	       gpointer client_data)
{
  /*get the prefix for our config */
  char *prefix = gnome_client_get_config_prefix (client);

  /*this is a "discard" command for discarding data from
     a saved session, usually this will work */
  char *argv[] = { "rm", "-r", NULL };

  /* Save the state using gnome-config stuff. */
  gnome_config_push_prefix (prefix);

  /* Here we will save our game to this special file, for reloading
   * on next restart */
  /* TODO */
/*       gnome_config_set_int("Section/Key",some_value);  */

  gnome_config_pop_prefix ();
  gnome_config_sync ();

  /* Here is the real SM code. We set the argv to the
     parameters needed to restart/discard the session that
     we've just saved and call the
     gnome_session_set_*_command to tell the session
     manager it. */
  argv[2] = gnome_config_get_real_path (prefix);
  gnome_client_set_discard_command (client, 3, argv);

  /* Set commands to clone and restart this application.
     Note that we use the same values for both -- the
     session management code will automatically add
     whatever magic option is required to set the session
     id on startup. The client_data was set to the
     command used to start this application when
     save_yourself handler was connected. */
  argv[0] = (gchar *) client_data;
  gnome_client_set_clone_command (client, 1, argv);
  gnome_client_set_restart_command (client, 1, argv);

  return TRUE;
}

void
die (GnomeClient * client, gpointer client_data)
{
  /* Just exit in a friendly way.  We don't need to
     save any state here, because the session manager
     should have sent us a save_yourself-message
     before.  */
  gtk_exit (0);
}
