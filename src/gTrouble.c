/* gTrouble.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gTrouble.h"
#include "main.h"
#include "misc.h"

GtkWidget *window;
GtkWidget *status;
GtkWidget *text;
GtkWidget *orig_xml;
GtkWidget *html_widget = NULL;
guideHtmlDocument *guide_pages;
int current_page = 0;

char *urls[] = {
  "unknown", "named (...)", "jump (#...)",
  "file_local (file.html)", "file_remote (file://foo.bar/file)",
  "ftp", "http", "secure_http", "gopher", "wais", "news", "telnet",
  "mailto", "exec:foo_bar", "pipe", "about", "info", "man", "form_image"
};

char *script_error =
  "<html><body><H2>There was an error processing a test script</H2>"
  "<p>The script returned an unexpected value.</p>"
  "<p>You'll have to follow the guide manually instead.</p>"
  "<p>Please report this problem to the Guide author</p>" "</body></html>";

char *guide_error =
  "<html><body><H2>There was an error processing this guide</H2>"
  "<p>This guide contains an error of some kind.</p>"
  "<p>Please report this problem to the Guide author</p>" "</body></html>";

int
main (int argc, char *argv[])
{
  GnomeClient *client;

  /* Initialise i18n 
     bindtextdomain (PACKAGE, GNOMELOCALEDIR);
     textdomain (PACKAGE);
   */

  /* ------------------------------------------------------- */
  /* Initialize GNOME */
  gnome_init ("gTrouble", VERSION, argc, argv);

  /* Initialise random numbers */
  srand (time (0));

  /* Session Management */

  /* Get the master client, that was hopefully connected to the
     session manager int the 'gnome_init' call.  All communication
     to the session manager will be done with this master client. */
  client = gnome_master_client ();

  /* Arrange to be told when something interesting happens.  */
  gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
		      GTK_SIGNAL_FUNC (save_yourself), (gpointer) argv[0]);
  gtk_signal_connect (GTK_OBJECT (client), "die",
		      GTK_SIGNAL_FUNC (die), NULL);

  /*check if we are connected to a session manager */
  if (GNOME_CLIENT_CONNECTED (client))
    {
      /*we are connected, we will get the prefix under which
         we saved our session last time and load up our data */
      gnome_config_push_prefix (gnome_client_get_config_prefix (client));

      TOMTRACE (("We are connected to the session manager\n"));
      /* We will eventually save our data to this prefix when the
       * session terminates, and load it back here, to resume a game.
       */
      /*         some_value = gnome_config_get_int("Section/Key=0");  */

      gnome_config_pop_prefix ();
    }
  else
    {
      /*we are not connected to any session manager, here we
         will just initialize a new game like we would normally
         do without a session manager */
      TOMTRACE (("We are NOT connected to the session manager\n"));
    }

  /* Create Instance of The App */
  window = gnome_app_new ("gTrouble", "gTrouble");

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      (GtkSignalFunc) destroy, NULL);
  gtk_window_set_policy (GTK_WINDOW (window), TRUE, TRUE, FALSE);
  gtk_container_set_border_width (GTK_CONTAINER (window), 0);

  /* Create Menu items */
  if (debug)
    printf ("About to load the menu-bar and status bar\n");
  status = gnome_appbar_new (FALSE, TRUE, GNOME_PREFERENCES_NEVER);
  gnome_app_set_statusbar (GNOME_APP (window), status);
  install_menus_and_toolbar (window);
  if (debug)
    printf ("I have loaded the menu-bar and status bar\n");

  TOMTRACE (("About to load user prefs\n"));

  options.propwindow = NULL;
  options.font_entry = NULL;

  load_user_preferences ();

  buildui ();

  load_xml ();

  /* This Positions the window in the top-left and sizes it */
  gtk_widget_set_uposition (window, 0, 0);
  gtk_widget_set_usize (window, 1000, 700);

  /* This is to show the window */
  gtk_widget_show (window);

  /* All GTK applications must have a gtk_main(). Control ends here
   * and waits for an event to occur (like a key press or
   * mouse event). */
  gtk_main ();
  return (0);
}

void
destroy (GtkWidget * widget, gpointer data)
{
  save_user_preferences ();
  gtk_main_quit ();
}

void
set_tooltip (GtkWidget * w, const gchar * tip)
{
  GtkTooltips *t = gtk_tooltips_new ();

  gtk_tooltips_set_tip (t, w, tip, NULL);
}


void
buildui (void)
{
  GtkWidget *vbox2, *hbox;
  GtkWidget *main_vbox;
  GtkWidget *button, *button2, *button3, *button4, *button5;
  GtkWidget *vscrollbar, *table, *table2, *vscrollbar2;

  vbox2 = gtk_vbox_new (FALSE, 0);
  hbox = gtk_hbox_new (TRUE, 0);
  main_vbox = gtk_vbox_new (FALSE, 0);
  button = gtk_button_new_with_label ("Parse lefthand xml into right window");
  button2 =
    gtk_button_new_with_label ("Parse lefthand xml and create some html"
			       " in a subdir of the current directory");
  button3 =
    gtk_button_new_with_label ("Parse lefthand xml, create some html"
			       " and send it to a gtk-xmHtml widget");
  button4 =
    gtk_button_new_with_label ("Parse lefthand xml, create some html"
			       " files using gnome-xml");
  button5 =
    gtk_button_new_with_label
    ("Create an index page from the contents of $GUIDEPATH");

  text = gtk_text_new (NULL, NULL);
  gtk_text_set_editable (GTK_TEXT (text), TRUE);

  orig_xml = gtk_text_new (NULL, NULL);
  gtk_text_set_editable (GTK_TEXT (orig_xml), TRUE);

  gtk_widget_show (button);
  gtk_widget_show (button2);
  gtk_widget_show (button3);
/*  gtk_widget_show (button4);  */
  gtk_widget_show (button5);
  gtk_widget_show (vbox2);
  gtk_widget_show (hbox);
  gtk_widget_show (main_vbox);

  gnome_app_set_contents (GNOME_APP (window), main_vbox);
  gtk_box_pack_end (GTK_BOX (main_vbox), vbox2, TRUE, TRUE, 0);
  gtk_box_pack_end (GTK_BOX (vbox2), button5, FALSE, FALSE, 0);
  gtk_box_pack_end (GTK_BOX (vbox2), button4, FALSE, FALSE, 0);
  gtk_box_pack_end (GTK_BOX (vbox2), button3, FALSE, FALSE, 0);
  gtk_box_pack_end (GTK_BOX (vbox2), button2, FALSE, FALSE, 0);
  gtk_box_pack_end (GTK_BOX (vbox2), button, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox, TRUE, TRUE, 0);

  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (parse_xml), NULL);

  gtk_signal_connect (GTK_OBJECT (button2), "clicked",
		      GTK_SIGNAL_FUNC (create_html), NULL);

  gtk_signal_connect (GTK_OBJECT (button3), "clicked",
		      GTK_SIGNAL_FUNC (parse_to_html_string), NULL);

  gtk_signal_connect (GTK_OBJECT (button4), "clicked",
		      GTK_SIGNAL_FUNC (create_html_xml), NULL);

  gtk_signal_connect (GTK_OBJECT (button5), "clicked",
		      GTK_SIGNAL_FUNC (create_index_html), NULL);

  table = gtk_table_new (2, 2, FALSE);
  gtk_table_set_row_spacing (GTK_TABLE (table), 0, 2);
  gtk_table_set_col_spacing (GTK_TABLE (table), 0, 2);
  gtk_box_pack_end (GTK_BOX (hbox), table, TRUE, TRUE, 0);
  gtk_widget_show (table);

  table2 = gtk_table_new (2, 2, FALSE);
  gtk_table_set_row_spacing (GTK_TABLE (table2), 0, 2);
  gtk_table_set_col_spacing (GTK_TABLE (table2), 0, 2);
  gtk_box_pack_start (GTK_BOX (hbox), table2, TRUE, TRUE, 0);
  gtk_widget_show (table2);

  gtk_table_attach (GTK_TABLE (table), text, 0, 1, 0, 1,
		    GTK_EXPAND | GTK_SHRINK | GTK_FILL,
		    GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 0);
  gtk_widget_show (text);

  gtk_table_attach (GTK_TABLE (table2), orig_xml, 0, 1, 0, 1,
		    GTK_EXPAND | GTK_SHRINK | GTK_FILL,
		    GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 0);
  gtk_widget_show (orig_xml);

  /* Add a vertical scrollbar to the GtkText widget */
  vscrollbar = gtk_vscrollbar_new (GTK_TEXT (text)->vadj);
  gtk_table_attach (GTK_TABLE (table), vscrollbar, 1, 2, 0, 1,
		    GTK_FILL, GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 0);
  gtk_widget_show (vscrollbar);

  vscrollbar2 = gtk_vscrollbar_new (GTK_TEXT (orig_xml)->vadj);
  gtk_table_attach (GTK_TABLE (table2), vscrollbar2, 1, 2, 0, 1,
		    GTK_FILL, GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 0);
  gtk_widget_show (vscrollbar2);
}

void
parse_xml (void)
{
  gguidePtr cur;
  gchar *tempxml;
  gtk_text_set_point (GTK_TEXT (text), 0);
  gtk_text_forward_delete (GTK_TEXT (text),
			   gtk_text_get_length (GTK_TEXT (text)));

  tempxml = gtk_editable_get_chars (GTK_EDITABLE (orig_xml), 0, -1);

  cur = parse_gguide_mem (tempxml);
  if (cur)
    {
      handle_gguide (cur);
      guide_free_gguide (cur);
    }
  else
    printf ("Aborting\n");

  g_free (tempxml);
}

void
create_html (void)
{
  gguidePtr cur;
  gchar *tempxml;

  tempxml = gtk_editable_get_chars (GTK_EDITABLE (orig_xml), 0, -1);

  cur = parse_gguide_mem (tempxml);

  if (cur)
    {
      html_out_gguide (cur);
      guide_free_gguide (cur);
    }
  else
    printf ("Aborting\n");

  g_free (tempxml);
}


void
create_html_xml (void)
{
  gguidePtr cur;
  gchar *tempxml;

  tempxml = gtk_editable_get_chars (GTK_EDITABLE (orig_xml), 0, -1);

  cur = parse_gguide_mem (tempxml);

  if (cur)
    {
      guide_export_to_html_files_gguide (cur);
      guide_free_gguide (cur);
    }
  else
    printf ("Aborting\n");

  g_free (tempxml);
}

void
handle_gguide (gguidePtr cur)
{
  int i;

  printf ("%d Guides registered\n", cur->nbguides);
  for (i = 0; i < cur->nbguides; i++)
    print_guide (cur->guides[i]);
}

void
parse_to_html_string (void)
{
  gguidePtr cur;
  gchar *tempxml;

  tempxml = gtk_editable_get_chars (GTK_EDITABLE (orig_xml), 0, -1);

  cur = parse_gguide_mem (tempxml);

  if (cur)
    {
      create_html_string_gguide (cur);
      /* guide_free_gguide (cur); */
    }
  else
    printf ("Aborting\n");

  g_free (tempxml);
}


void
create_html_string_gguide (gguidePtr cur)
{
/*  int i;
  gchar *buffer; */
  static guideHtmlDocument *mydoc;
  GUIDETRACE (("create_html_string_gguide\n"));

  mydoc = guide_create_html_document (cur->guides[0]);
  send_html_to_widget (mydoc);
}

void
send_html_to_widget (guideHtmlDocument * ret)
{
  show_html_guide (ret);
}

void
show_html_guide (guideHtmlDocument * doc)
{

  guide_pages = doc;

  html_widget = create_xmhtml_window ();

  current_page = 0;
  TOMTRACE (("About to source:\n%s\n", doc->pages[0]->str));
  gtk_xmhtml_source (GTK_XMHTML (html_widget), doc->pages[0]->str);

}


GtkWidget *
create_xmhtml_window (void)
{
  GtkWidget *html_window = NULL;
  GtkWidget *table = NULL;

  html_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  html_widget = gtk_xmhtml_new ();

  table = gtk_table_new (2, 2, FALSE);

  gtk_container_add (GTK_CONTAINER (html_window), table);
  gtk_table_attach_defaults (GTK_TABLE (table), html_widget, 0, 1, 0, 1);

  gtk_widget_show (table);
  gtk_widget_show (html_widget);
  gtk_widget_set_usize (html_window, 600, 600);

  gtk_xmhtml_set_anchor_underline_type (GTK_XMHTML (html_widget),
					GTK_ANCHOR_SINGLE_LINE);
  gtk_xmhtml_set_anchor_buttons (GTK_XMHTML (html_widget), FALSE);
  gtk_xmhtml_set_hilight_on_enter (GTK_XMHTML (html_widget), TRUE);

  gtk_widget_show (html_window);


  gtk_signal_connect (GTK_OBJECT (html_widget), "activate",
		      (GtkSignalFunc) xmhtml_click, html_widget);
  return html_widget;
}

void
xmhtml_click (GtkWidget * widget, gpointer data)
{
  XmHTMLAnchorCallbackStruct *cbs = (XmHTMLAnchorCallbackStruct *) data;
  int i = 0;

  printf ("click!\n");
  printf ("URLtype: %s\n", urls[cbs->url_type]);
  printf ("line:    %d\n", cbs->line);
  printf ("href:    %s\n", cbs->href);
  printf ("target:  %s\n", cbs->target);
  printf ("rel:     %s\n", cbs->rel);
  printf ("rev:     %s\n", cbs->rev);
  printf ("title:   %s\n", cbs->title);
  printf ("doit:    %d\n", cbs->doit);

  /* I've pinched the pipe: prefix, I hope nobody misses it! */
  if ((cbs->url_type == ANCHOR_PIPE) && (strcmp (cbs->href, "pipe:-1")))
    {
      i = atoi (cbs->href + 5);
      if (i == -1)
	{
	  gtk_xmhtml_source (GTK_XMHTML (widget), guide_error);
	  return;
	}
      TOMTRACE (("About to source:\n%s\n", guide_pages->pages[i]->str));
      current_page = i;
      gtk_xmhtml_source (GTK_XMHTML (widget), guide_pages->pages[i]->str);
    }
  /* External links, use moz */
  else if ((cbs->url_type == ANCHOR_FILE_REMOTE)
	   || (cbs->url_type == ANCHOR_FTP) || (cbs->url_type == ANCHOR_HTTP)
	   || (cbs->url_type == ANCHOR_SECURE_HTTP)
	   || (cbs->url_type == ANCHOR_MAILTO)
	   || (cbs->url_type == ANCHOR_NEWS))
    {
      gchar *sys =
	g_strdup_printf ("gnome-moz-remote --newwin %s", cbs->href);
      system (sys);
      g_free (sys);
    }
  /* Test href. There may be a cleaner way to do this in future... */
  else if ((cbs->url_type == ANCHOR_UNKNOWN)
	   && (strstr (cbs->href, "guidetest:")))
    {
      int j = 0;
      printf ("Test! Language=%s\n", cbs->href + 10);
      printf ("Test script is:\n%s\n",
	      guide_pages->guide_struct->questions[current_page -
						   1]->test->script);
      j = do_test_return_node_ref (cbs->href + 10,
				   guide_pages->
				   guide_struct->questions[current_page -
							   1]->test->script,
				   guide_pages->guide_struct);
      if (j == -1)
	{
	  gtk_xmhtml_source (GTK_XMHTML (widget), script_error);
	  return;
	}
      gtk_xmhtml_source (GTK_XMHTML (widget), guide_pages->pages[j]->str);
    }
  else if ((cbs->url_type == ANCHOR_UNKNOWN)
	   && (strstr (cbs->href, "guide:")))
    {
      guideHtmlDocument *mydoc;
      gguidePtr cur = NULL;

      cur = parse_gguide_file (cbs->href + 6);

      mydoc = guide_create_html_document (cur->guides[0]);

      guide_pages = mydoc;

      current_page = 0;
/*      TOMTRACE (("About to source:\n%s\n", mydoc->pages[0]->str)); */
      gtk_xmhtml_source (GTK_XMHTML (html_widget), mydoc->pages[0]->str);
    }
}


int
do_test_return_node_ref (gchar * lang, gchar * script, guidePtr cur)
{
  gchar *argv[6];
  guchar *outbuf;
  gint outbuflen;
  gint i = 0;
  int ret = -1;

  if (!strcmp (lang, "perl"))
    argv[i++] = "perl";
  else if (!strcmp (lang, "sh"))
    argv[i++] = "/bin/sh";
  else
    return -1;

  argv[i++] = NULL;
  getOutputFrom (argv, script, strlen (script), &outbuf, &outbuflen);

  fprintf (stdout, "OutputFrom returned:\n-->%s<--\n", outbuf);

  /* Skip leading newlines, if any. */
  while (*outbuf == '\n')
    outbuf++;

  /* Skip tailing newlines, if any. */
  for (i = 0; i < strlen (outbuf); i++)
    if (outbuf[i] == '\n')
      outbuf[i] = '\0';

  fprintf (stdout, "I've hacked it down to:\n-->%s<--\n", outbuf);

  for (i = 0; i < cur->nbquestions; i++)
    {
      if (!strcmp (cur->questions[i]->ref, outbuf))
	ret = cur->questions[i]->int_ref;
    }
  return ret;
}

int
do_test_return_node_ref_old (gchar * lang, gchar * script, guidePtr cur)
{
  int ret = -1;
  gchar *sys;
  FILE *pipe;
  char buf[256];
  char buf2[256];
  int i = 0;
  char temp[] = "./test_temp_XXXX";
  int tempfd;
  FILE *tempfile;

  tempfd = open (temp, O_CREAT | O_EXCL | O_TRUNC | O_RDWR, 0600);

  tempfile = fdopen (tempfd, "w");

  fprintf (tempfile, script);

  fclose (tempfile);

  if (!strcmp (lang, "perl"))
    sys = g_strdup_printf ("perl \"%s\"", temp);
  else if (!strcmp (lang, "sh"))
    sys = g_strdup_printf ("/bin/sh \"%s\"", temp);
  else
    return -1;

  printf ("About to open pipe to:\n%s\n", sys);
  if ((pipe = popen (sys, "r")) == NULL)
    return -1;

  fgets (buf, sizeof (buf), pipe);

  printf ("Recieved the respose:\n%s\n", buf);
  /* Discard remainder */
  while (fgets (buf2, sizeof (buf2), pipe) != 0)
    ;

  for (i = 0; i < cur->nbquestions; i++)
    {
      if (!strcmp (cur->questions[i]->ref, buf))
	ret = cur->questions[i]->int_ref;
    }

  unlink (temp);

  if (fclose (pipe))
    return -1;

  return ret;
}

gchar *
get_guide_path (void)
{
  static char def[] = "/usr/share/guides/";

  if (getenv ("GUIDEPATH"))
    {
      return getenv ("GUIDEPATH");
    }
  else
    {
      return def;
    }
}

gboolean create_index_html (void)
{
  GString *index_html = NULL;
  struct dirent *guideFile;
  struct stat guideStat;
  DIR *guideDir;

  guideDir = opendir (get_guide_path ());
  if (guideDir == NULL)
    return FALSE;

  index_html = g_string_new (NULL);

  html_string_create_headers (index_html, "Guide Index");

  guideFile = readdir (guideDir);
  while (guideFile != NULL)
    {

      if (strcmp (guideFile->d_name, ".") && strcmp (guideFile->d_name, ".."))
	{
	  TOMTRACE (("Stat-ing: %s\n", guideFile->d_name));
	  if (stat
	      (g_concat_dir_and_file (get_guide_path (), guideFile->d_name),
	       &guideStat))
	    {
	      TOMTRACE (("Error stat-ing file\n"));
	      return FALSE;
	    }

	  if (S_ISDIR (guideStat.st_mode))
	    {
	      /* Recurse? For now we'll ignore subdirs. */
	      TOMTRACE (("Found a directory, ignoring : %s\n",
			 guideFile->d_name));
	    }
	  else
	    {
	      TOMTRACE (("Testing FileName, its:%s\n", guideFile->d_name));
	      if (strstr (guideFile->d_name, ".guide"))
		{
		  TOMTRACE (("%s Is a Guide file!\n", guideFile->d_name));
		  add_guide_to_index (index_html,
				      g_concat_dir_and_file (get_guide_path
							     (),
							     guideFile->d_name));
		}
	    }
	}
      guideFile = readdir (guideDir);
    }

  html_string_create_footers (index_html);

  if ((closedir (guideDir) == -1))
    {
      TOMTRACE (("Error closing DIR Stream"));
      return FALSE;
    }

/*  TOMTRACE (("Index.html is:\n%s\n", index_html->str)); */
  html_widget = create_xmhtml_window ();
  gtk_xmhtml_source (GTK_XMHTML (html_widget), index_html->str);

  return TRUE;
}

void
add_guide_to_index (GString * index_html, gchar * guide_path)
{
  gguidePtr tempgguide;
  guidePtr tempguide;

  tempgguide = parse_gguide_file (guide_path);
  tempguide = tempgguide->guides[0];

  g_string_sprintfa (index_html, "<p>\n<h3>Application: %s</h3>\n",
		     tempguide->application);
  g_string_sprintfa (index_html, "<h4>Category: %s</h4>\n",
		     tempguide->category);
  g_string_sprintfa (index_html,
		     "<ul>\n<li><a href=\"guide:%s\">Topic: %s</a>\n</ul></p>",
		     guide_path, tempguide->topic);

  guide_free_gguide (tempgguide);
}
