/* menus.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gTrouble.h"

void exit_cb (void);

extern user_preferences options;
extern GtkWidget *window;
extern int debug;

static GnomeUIInfo file_menu[] = {
  GNOMEUIINFO_MENU_EXIT_ITEM (exit_cb, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo edit_menu[] = {
  GNOMEUIINFO_MENU_PROPERTIES_ITEM (show_preferences_dialog, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {
  GNOMEUIINFO_HELP ("gTrouble"),
  GNOMEUIINFO_MENU_ABOUT_ITEM (show_about_dialog, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo menu[] = {
  GNOMEUIINFO_MENU_FILE_TREE (file_menu),
  GNOMEUIINFO_MENU_SETTINGS_TREE (edit_menu),
  GNOMEUIINFO_MENU_HELP_TREE (help_menu),
  GNOMEUIINFO_END
};

/*
static GnomeUIInfo toolbar[] = {
  GNOMEUIINFO_ITEM_STOCK (N_ ("New"), N_ ("Start a new game"), new_game_cb,
			  GNOME_STOCK_PIXMAP_NEW),

  GNOMEUIINFO_END
};
*/

int
show_about_dialog (void)
{
  static GtkWidget *dialog = NULL;

  if (dialog != NULL)
    {
      g_assert (GTK_WIDGET_REALIZED (dialog));
      gdk_window_show (dialog->window);
      gdk_window_raise (dialog->window);
    }
  else
    {
      const gchar *authors[] = {
	"Tom Gilbert <gilbertt@tomgilbert.freeserve.co.uk>",
	NULL
      };
      gchar *logo = gnome_pixmap_file ("gTrouble.png");
      dialog = gnome_about_new (_ ("gTrouble"), VERSION,
				"(C) 1999 Tom Gilbert", authors,
				_ ("I hope you enjoy it."), logo);

      g_free (logo);
      gtk_signal_connect (GTK_OBJECT (dialog),
			  "destroy",
			  GTK_SIGNAL_FUNC (gtk_widget_destroyed), &dialog);

      gnome_dialog_set_parent (GNOME_DIALOG (dialog), GTK_WINDOW (window));
      gtk_widget_show (dialog);
    }

  return TRUE;
}

void
install_menus_and_toolbar (GtkWidget * app)
{
  /*   Not enough gubbins to warrant a toolbar yet :)
   *   gnome_app_create_toolbar_with_data (GNOME_APP (app), toolbar, app);
   */
  gnome_app_create_menus_with_data (GNOME_APP (app), menu, app);
  gnome_app_install_menu_hints (GNOME_APP (app), menu);
}

void
exit_cb (void)
{
  exit (0);
}
