/* display.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gTrouble.h"

extern GtkWidget *text;
extern GtkWidget *orig_xml;

void
load_xml (void)
{
  FILE *fp;
  gchar c[2] = { '\0', '\0' };
  fp = fopen (g_concat_dir_and_file(GUIDEDIR, "test.guide"), "r");
  if (fp == NULL)
    {
      printf ("Error opening file");
      gtk_exit (2);
    }

  gtk_text_freeze (GTK_TEXT (orig_xml));
  while ((c[0] = fgetc (fp)) != EOF)
    gtk_text_insert (GTK_TEXT (orig_xml), NULL, NULL, NULL, c, 1);
  gtk_text_thaw (GTK_TEXT (orig_xml));
  fclose (fp);
}

/* Print a person */
void
printPerson (personPtr cur)
{
  gchar buffer[2000];

  gtk_text_freeze (GTK_TEXT (text));

  if (cur == NULL)
    return;

  gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, "------ Person\n", -1);

  if (cur->type)
    {
      g_snprintf (buffer, sizeof (buffer), "     type: %s\n", cur->type);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }  
  if (cur->name)
    {
      g_snprintf (buffer, sizeof (buffer), "     name: %s\n", cur->name);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->email)
    {
      g_snprintf (buffer, sizeof (buffer), "    email: %s\n", cur->email);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->company)
    {
      g_snprintf (buffer, sizeof (buffer), "  company: %s\n", cur->company);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->organisation)
    {
      g_snprintf (buffer, sizeof (buffer), "     organisation: %s\n",
		  cur->organisation);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->snailmail)
    {
      g_snprintf (buffer, sizeof (buffer), "    snailmail: %s\n",
		  cur->snailmail);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->webpage)
    {
      g_snprintf (buffer, sizeof (buffer), "  Web: %s\n", cur->webpage);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->phone)
    {
      g_snprintf (buffer, sizeof (buffer), "    phone: %s\n", cur->phone);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, "------\n", -1);
  gtk_text_thaw (GTK_TEXT (text));
}


/* Print a question */
void
print_question (questionPtr cur)
{
  gchar buffer[2000];
  int i;

  gtk_text_freeze (GTK_TEXT (text));

  if (cur == NULL)
    return;
  gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL,
		   "~~~~~~~~~~~~~ Question\n", -1);
  if (cur->ref)
    {
      g_snprintf (buffer, sizeof (buffer), "     ref: %s\n", cur->ref);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
   if (cur->int_ref)
    {
      g_snprintf (buffer, sizeof (buffer), "     int_ref: %d\n", cur->int_ref);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
 if (cur->text)
    {
      g_snprintf (buffer, sizeof (buffer), "     text: %s\n", cur->text);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->tellme)
    {
      g_snprintf (buffer, sizeof (buffer), "    tellme: %s\n", cur->tellme);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->longtext)
    {
      g_snprintf (buffer, sizeof (buffer), "  longtext: %s\n", cur->longtext);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  
  if (cur->test)
    print_test (cur->test);

  g_snprintf (buffer, sizeof (buffer), "%d Choices\n", cur->nbchoices);
  gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);

  for (i = 0; i < cur->nbchoices; i++)
    print_choice (cur->choices[i]);

  gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, "~~~~~~~~~~~~~\n", -1);
  gtk_text_thaw (GTK_TEXT (text));
}



/* Print a test */
void
print_test (testPtr cur)
{
  gchar buffer[2000];

  gtk_text_freeze (GTK_TEXT (text));

  if (cur == NULL)
    return;
  gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, "--- Test\n", -1);
  if (cur->text)
    {
      g_snprintf (buffer, sizeof (buffer), "     text: %s\n", cur->text);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->lang)
    {
      g_snprintf (buffer, sizeof (buffer), "     lang: %s\n", cur->lang);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->script)
    {
      g_snprintf (buffer, sizeof (buffer), "    script: %s\n", cur->script);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }

  gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, "---\n", -1);

  gtk_text_thaw (GTK_TEXT (text));
}



/* Print a Choice */
void
print_choice (choicePtr cur)
{
  gchar buffer[2000];

  gtk_text_freeze (GTK_TEXT (text));

  if (cur == NULL)
    return;
  gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, "---- Choice\n", -1);
  if (cur->label)
    {
      g_snprintf (buffer, sizeof (buffer), "     label: %s\n", cur->label);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->value)
    {
      g_snprintf (buffer, sizeof (buffer), "     value: %s\n", cur->value);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->action)
    {
      g_snprintf (buffer, sizeof (buffer), "     action: %s\n", cur->action);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->target)
    {
      g_snprintf (buffer, sizeof (buffer), "    target: %s\n", cur->target);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, "----\n", -1);

  gtk_text_thaw (GTK_TEXT (text));
}


/* and to print it */
void
print_guide (guidePtr cur)
{
  int i;
  gchar buffer[1000];		/* Just temporary, as a demo */

  /* Freeze the text widget, ready for multiple updates */
  gtk_text_freeze (GTK_TEXT (text));

  if (cur == NULL)
    return;

  gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL,
		   "=========================  Guide\n", -1);

  if (cur->guideID)
    {
      g_snprintf (buffer, sizeof (buffer), "guideID: %s\n", cur->guideID);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->status)
    {
      g_snprintf (buffer, sizeof (buffer), "status: %s\n", cur->status);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->modified)
    {
      g_snprintf (buffer, sizeof (buffer), "modified: %s\n", cur->modified);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->application)
    {
      g_snprintf (buffer, sizeof (buffer), "application: %s\n",
		  cur->application);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->category)
    {
      g_snprintf (buffer, sizeof (buffer), "category: %s\n", cur->category);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->topic)
    {
      g_snprintf (buffer, sizeof (buffer), "topic: %s\n", cur->topic);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }
  if (cur->intro)
    {
      g_snprintf (buffer, sizeof (buffer), "intro: %s\n", cur->intro);
      gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);
    }

  g_snprintf (buffer, sizeof (buffer), "%d Contacts:\n", cur->nbcontacts);
  gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);

  for (i = 0; i < cur->nbcontacts; i++)
    printPerson (cur->contacts[i]);

  g_snprintf (buffer, sizeof (buffer), "%d questions\n", cur->nbquestions);
  gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buffer, -1);

  for (i = 0; i < cur->nbquestions; i++)
    print_question (cur->questions[i]);

  gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL,
		   "=================== \n", -1);

  /* Thaw the text widget, allowing the updates to become visible */
  gtk_text_thaw (GTK_TEXT (text));
}
