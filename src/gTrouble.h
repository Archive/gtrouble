/* gTrouble.h
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*** gTrouble_h */
#ifndef GTROUBLE_H
#define GTROUBLE_H

#include "config.h"
#include <gnome.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <gtk/gtk.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include "gnome-xml/parser.h"
#include "../libguide/libguide.h"
#include "gtk-xmhtml/gtk-xmhtml.h"

#undef TOMTRACE
#define TOMTRACE(a) { \
    if(debug) \
    { \
    printf("%s +%u : ",__FILE__,__LINE__); \
            printf a; \
	    fflush(stdout); \
    } \
}

typedef struct
{
  /* The property window widgets */
  GtkWidget *propwindow;
  GtkWidget *font_entry;
}
user_preferences;

/* ****** FUNCTIONS ******** */
void destroy (GtkWidget * widget, gpointer data);
int load_user_preferences (void);
int save_user_preferences (void);

void show_preferences_dialog (GtkWidget * widget, gpointer data);
int show_about_dialog (void);
void die (GnomeClient * client, gpointer client_data);
int save_yourself (GnomeClient * client, int phase,
		   GnomeSaveStyle save_style, int shutdown,
		   GnomeInteractStyle interact_style, int fast,
		   gpointer client_data);
void font_sel_ok (GtkWidget * w, GtkWidget * fsel);
void font_sel_cancel (GtkWidget * w, GtkWidget * fsel);
void font_button_pressed (GtkWidget * w, gpointer data);
void set_tooltip (GtkWidget * w, const gchar * tip);
void install_menus_and_toolbar (GtkWidget * app);

void print_guide (guidePtr cur);
void print_question (questionPtr cur);
void printPerson (personPtr cur);
void print_choice (choicePtr cur);
void print_test (testPtr cur);

void load_xml (void);
void parse_xml (void);
void create_html (void);
void create_html_xml (void);
void handle_gguide (gguidePtr cur);
void buildui (void);
void send_html_to_widget (guideHtmlDocument* pages);
void create_html_string_gguide(gguidePtr cur);
void parse_to_html_string(void);
void xmhtml_click (GtkWidget * widget, gpointer data);
int do_test_return_node_ref(gchar * lang, gchar * script, guidePtr cur);
void create_html_string_gguide (gguidePtr cur);

int do_test_return_node_ref_old (gchar * lang, gchar * script, guidePtr cur);
gchar *get_guide_path (void);
gboolean create_index_html (void);
void add_guide_to_index (GString * index_html, gchar * guide_path);
GtkWidget *create_xmhtml_window (void);
void show_html_guide (guideHtmlDocument * doc);


/* ************************************* */

#endif

/* gTrouble_h ***/
