/* properties.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gTrouble.h"

extern int debug;
extern user_preferences options;
extern user_preferences old_options;
void property_apply_cb (GtkWidget * widget, void *nodata, gpointer data);
gint property_destroy_cb (GtkWidget * widget, gpointer data);

void
property_apply_cb (GtkWidget * widget, void *nodata, gpointer data)
{
  user_preferences *ad = &options;
  user_preferences *old = &old_options;
  gchar *buf;
  gint info_changed = FALSE;

  if (info_changed)
    {
      TOMTRACE (("User preferences saved\n"));
      save_user_preferences ();
      /* Re-copy options for next comparison */
      TOMTRACE (("About to memcpy options again\n"));
      memcpy (&old_options, &options, sizeof (user_preferences));
    }
  else
    TOMTRACE (("User preferences not changed\n"));

}

gint property_destroy_cb (GtkWidget * widget, gpointer data)
{
  options.propwindow = NULL;
  return FALSE;
}

void
show_preferences_dialog (GtkWidget * widget, gpointer data)
{
  user_preferences *ad = &options;
  GtkWidget *frame;
  GtkWidget *pref_vbox;
  GtkWidget *pref_hbox;
  GtkWidget *label;
  GtkWidget *button;
  GtkWidget *button2;
  GtkWidget *font_button;
  GtkObject *adj;

  if (ad->propwindow)
    {
      TOMTRACE (("propwindow is already open, I'm showing it..."));
      gdk_window_raise (ad->propwindow->window);
      return;
    }

  /* Grab old property settings for comparison */
  TOMTRACE (("About to memcpy options\n"));
  memcpy (&old_options, &options, sizeof (user_preferences));

  ad->propwindow = gnome_property_box_new ();
  gtk_window_set_title (GTK_WINDOW
			(&GNOME_PROPERTY_BOX (ad->propwindow)->dialog.window),
			_ ("gTrouble Settings"));

  TOMTRACE (("Created new property window\n"));

  pref_vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
  gtk_container_set_border_width (GTK_CONTAINER (pref_vbox), GNOME_PAD_SMALL);

  label = gtk_label_new (_ ("Settings"));
  gtk_widget_show (pref_vbox);
  gnome_property_box_append_page (GNOME_PROPERTY_BOX (ad->propwindow),
				  pref_vbox, label);

  gtk_signal_connect (GTK_OBJECT (ad->propwindow), "apply",
		      GTK_SIGNAL_FUNC (property_apply_cb), ad);
  gtk_signal_connect (GTK_OBJECT (ad->propwindow), "destroy",
		      GTK_SIGNAL_FUNC (property_destroy_cb), ad);

  gtk_widget_show_all (ad->propwindow);
}


int
load_user_preferences (void)
{
/* Save ourselves some typing :) */
  gnome_config_push_prefix ("/gTrouble/settings/");

  gnome_config_pop_prefix ();

  return TRUE;
}

int
save_user_preferences (void)
{

  /* Save ourselves some typing :) */
  gnome_config_push_prefix ("/gTrouble/settings/");

  gnome_config_sync ();

  gnome_config_pop_prefix ();

  return TRUE;
}

void
font_button_pressed (GtkWidget * w, gpointer data)
{
  GtkWidget *fs;

  TOMTRACE (("Font Button Pressed\n"));

  fs = gtk_font_selection_dialog_new ("Select Tile Font");
  gtk_font_selection_dialog_set_font_name (GTK_FONT_SELECTION_DIALOG (fs),
					   gtk_entry_get_text (GTK_ENTRY
							       (options.
								font_entry)));

  gtk_signal_connect (GTK_OBJECT (GTK_FONT_SELECTION_DIALOG (fs)->ok_button),
		      "clicked", GTK_SIGNAL_FUNC (font_sel_ok), fs);
  gtk_signal_connect (GTK_OBJECT
		      (GTK_FONT_SELECTION_DIALOG (fs)->cancel_button),
		      "clicked", GTK_SIGNAL_FUNC (font_sel_cancel), fs);

  gtk_widget_show (fs);
}

void
font_sel_ok (GtkWidget * w, GtkWidget * fsel)
{
  gtk_entry_set_text (GTK_ENTRY (options.font_entry),
		      gtk_font_selection_dialog_get_font_name
		      (GTK_FONT_SELECTION_DIALOG (fsel)));
  gtk_widget_destroy (fsel);
}

void
font_sel_cancel (GtkWidget * w, GtkWidget * fsel)
{
  gtk_widget_destroy (fsel);
}
