/* gTextGuide.h
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*** gTextGuide_h */
#ifndef GTEXTGUIDE_H
#define GTEXTGUIDE_H

#include "config.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <ctype.h>
#include "../libguide/libguide.h"

#define TOMTRACE(a) { \
    if(debug) \
    { \
    printf("%s +%u : ",__FILE__,__LINE__); \
            printf a; \
	    fflush(stdout); \
    } \
}

/* ****** FUNCTIONS ******** */

void print_guide (guidePtr cur);
void print_question (questionPtr cur);
void printPerson (personPtr cur);
void print_choice (choicePtr cur);
void print_test (testPtr cur);

void print_xml (void);
void create_html (void);
void print_gguide (gguidePtr cur);

/* ************************************* */

#endif

/* gTextGuide_h ***/
