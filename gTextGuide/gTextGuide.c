/* gTextGuide.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gTextGuide.h"
#include "main.h"

int
main (int argc, char *argv[])
{
  int i = 0;
  int output_html=0;
  int print_text=0;
  /* Initialise i18n 
     bindtextdomain (PACKAGE, GNOMELOCALEDIR);
     textdomain (PACKAGE);
   */

  if (argc < 2)
    {
      printf ("Usage: %s -p -o\n"
	      "Where -p prints output to stdout, and\n"
	      "      -o creates html files from guide content\n", argv[0]);
      exit (1);
    }

  for (i = 1; i < argc; i++)
    {
      if (!strcmp (argv[i], "-p"))
	  print_text=TRUE;
      else if (!strcmp (argv[i], "-o"))
	  output_html=TRUE;
    }

  if(print_text)
	print_xml();
  if(output_html)
	create_html();

  return (0);
}

void
print_xml (void)
{
  gguidePtr cur;

  cur = parse_gguide_file (GUIDEDIR "test.guide");
  if (cur)
    {
      print_gguide (cur);
      guide_free_gguide (cur);
    }
  else
    printf ("Aborting\n");
}

void
create_html (void)
{
  gguidePtr cur;

  cur = parse_gguide_file ("../libguide/test.xml");

  if (cur)
    {
      html_out_gguide (cur);
      guide_free_gguide (cur);
    }
  else
    printf ("Aborting\n");
}

void
print_gguide (gguidePtr cur)
{
  int i;

  printf ("%d Guides registered\n", cur->nbguides);
  for (i = 0; i < cur->nbguides; i++)
    print_guide (cur->guides[i]);
}
