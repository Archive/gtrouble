/* display.c
 *
 * Copyright (C) 1999 Tom Gilbert
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gTrouble.h"

/* Print a person */
void
printPerson (personPtr cur)
{
  if (cur == NULL)
    return;

  printf("------ Person\n");

  if (cur->type)
      printf ("     type: %s\n", cur->type);
  if (cur->name)
      printf ("     name: %s\n", cur->name);
  if (cur->email)
      printf ("    email: %s\n", cur->email);
  if (cur->company)
      printf ("  company: %s\n", cur->company);
  if (cur->organisation)
      printf ("  organisation: %s\n", cur->organisation);
  if (cur->snailmail)
      printf ("    snailmail: %s\n", cur->snailmail);
  if (cur->webpage)
      printf ("  Web: %s\n", cur->webpage);
  if (cur->phone)
      printf ("    phone: %s\n", cur->phone);
}


/* Print a question */
void
print_question (questionPtr cur)
{
  int i;

  if (cur == NULL)
    return;
  
  printf("~~~~~~~~~~~~~ Question\n");
  
  if (cur->ref)
      printf ("     ref: %s\n", cur->ref);
  if (cur->int_ref)
      printf ("  int_ref: %d\n", cur->int_ref);
  if (cur->text)
      printf ("    text: %s\n", cur->text);
  if (cur->tellme)
      printf ("   tellme: %s\n", cur->tellme);
   if (cur->longtext)
      printf ("   longtext: %s\n", cur->longtext);

  if (cur->test)
    print_test (cur->test);

  printf ("%d Choices\n", cur->nbchoices);

  for (i = 0; i < cur->nbchoices; i++)
    print_choice (cur->choices[i]);

  printf ("~~~~~~~~~~~~~\n");
}


/* Print a test */
void
print_test (testPtr cur)
{
  if (cur == NULL)
    return;

  printf("--- Test\n");
  if (cur->text)
      printf ("     text: %s\n", cur->text);
  if (cur->lang)
      printf ("     lang: %s\n", cur->lang);
  if (cur->script)
      printf ("    script: %s\n", cur->script);

  printf("---\n");
}



/* Print a Choice */
void
print_choice (choicePtr cur)
{
  if (cur == NULL)
    return;

  printf("---- Choice\n");
  if (cur->label)
      printf ("     label: %s\n", cur->label);
  if (cur->value)
      printf ("     value: %s\n", cur->value);
  if (cur->action)
      printf ("    action: %s\n", cur->action);
  if (cur->target)
      printf ("    target: %s\n", cur->target);

  printf("----\n");
}


/* Code to print a guide */
void
print_guide (guidePtr cur)
{
  int i;

  if (cur == NULL)
    return;

  printf("=========================  Guide\n");

  if (cur->guideID)
      printf ("guideID: %s\n", cur->guideID);
  if (cur->status)
      printf ("status: %s\n", cur->status);
  if (cur->modified)
      printf ("modified: %s\n", cur->modified);
  if (cur->application)
      printf ("application: %s\n", cur->application);
  if (cur->category)
      printf ("category: %s\n", cur->category);
  if (cur->topic)
      printf ("topic: %s\n", cur->topic);
  if (cur->intro)
      printf ("intro: %s\n", cur->intro);

  printf ("%d Contacts:\n", cur->nbcontacts);

  for (i = 0; i < cur->nbcontacts; i++)
    printPerson (cur->contacts[i]);

  printf ("%d questions\n", cur->nbquestions);

  for (i = 0; i < cur->nbquestions; i++)
    print_question (cur->questions[i]);

  printf("=================== \n");
}
